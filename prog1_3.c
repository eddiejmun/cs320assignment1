#include <stdio.h>
#include <stdlib.h>
#include "prog1_2.h"
#include <string.h>

int main(int argc, char *argv[]) {
    printf("Assignment #1-3, Edward Mun, eddiejmun@gmail.com\n");
    
    if(argc != 2) {
        printf("This program expects a single command line argument.\n");
        exit(0);
    }
 
    struct stack *builtStack = MakeStack(100);
    int N = atoi(argv[1]);
    for(int i=0; i<N; i++) {
        char func[256];
        char num[256];
        char buff[256];
        char input[256];
        int val;
        char *push = "push";
        char *pop = "pop";
        printf(">");
        scanf("%[^\n]s",buff);
        while ((getchar()) != '\n');
        strcpy(input,buff);
        char *token = strtok(input," ");
        int counter = 0;
        while (token && counter <= 2) {
            if (counter == 0) {
                strcpy(func,token);
            }
            if (counter == 1) {
                strcpy(num,token);
            }
       
            counter++;
            token = strtok(NULL," ");
        }  
        if((strcmp(func, push)) == 0) {
            if (counter == 2) {
                val = atoi(num);
                Push(builtStack,val);
            }
        }  
        if((strcmp(func, pop)) == 0) {
            if(counter == 1) {
                printf("%d\n", Pop(builtStack));
            }
        }      
}
}
    
    
