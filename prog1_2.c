#include <stdio.h>
#include <stdlib.h>
#include "prog1_2.h"

STACK* MakeStack(int initialCapacity) {
	struct stack *newStack = (struct stack*)malloc(sizeof(struct stack)*1);
	newStack->size = 0;
	newStack->capacity = initialCapacity;
	newStack->data = (int*)malloc(sizeof(int)*1000);
	return newStack;
}

void Push(STACK *stackPtr, int data) {
	if(stackPtr->size > stackPtr->capacity) {
        Grow(stackPtr);
    }
	stackPtr->data[stackPtr->size] = data;
	stackPtr->size++;
}

int Pop(STACK *stackPtr) {
    if(stackPtr->size == 0) {
        return -1;
    }
    stackPtr->size--;
    return stackPtr->data[stackPtr->size];
}

void Grow(STACK *stackPtr) {
    stackPtr->capacity = stackPtr->capacity*2;
}

    
	

